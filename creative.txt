Group numbers:
	Shaoyang Zhou	440747
	Runbo Yang	439275

Link:
	http://ec2-54-68-190-75.us-west-2.compute.amazonaws.com/~zhousy/greeting.html

Description:
	If you login with the username “TA”, you’ll be regard as a superuser who has the permission to see all the files of all the users. And the TA user can also open or delete any files you want. Of course, TA user has a private space to store his own file. The different between his own file and the other users’ file is the tag. The “filename” means the other users’ file, the tag my file is his own file.

Details:
	normal users:	zhousy
			runbo
			adminastrator
			TA