<!DOCTYPE html>
<html>
<head>
    <title>File Sharing System</title>
</head>

<body>
    <h4>Processing</h4>
    <?php
    session_start();    
    $username = $_SESSION['username'];
    printf("<p>You are: %s</p>\n",
	htmlentities($username));
    $action = $_GET['action'];
    if ($action == "open"){
        $filename = $_GET['selection'];
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename";
            exit;
        }
        $full_path = sprintf("/home/zhousy/security/uploads/%s/%s", $username, $filename);
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($full_path);
        header('Content-Type: '.$mime);
        readfile($full_path);
    } elseif($action == "upload"){
        echo '<form enctype="multipart/form-data" action="upupup.php" method="POST">
	<p>
	    <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
	    <label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
	    <input type="submit" value="Upload File" />
	</p>
        </form>';
    } elseif($action == "delete"){
        $filename = $_GET['selection'];
        if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
            echo "Invalid filename";
            exit;
        }
        $full_path = "/home/zhousy/security/uploads/" . $username . "/" . $filename;
        if(unlink($full_path)){
            echo "Successfully Deleted";
        } else{
	    //echo $full_path;
            echo "Failed";
        }
    } else{
        session_destroy();
        header("Location: greeting.html");
        exit;
    }
    echo '<br><input type="button" value="Back" onclick="location.href=\'mainpage.php\'" />';
    ?>
</body>
</html>